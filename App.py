from flask import Flask,render_template,request,redirect,url_for,flash
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.secret_key = "Secret Key"


app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:''@localhost/project_db'
app.config['SQLALCHEMY_TRACK_MODIFICATION'] = False

db = SQLAlchemy(app)


class Role(db.Model):
    __tablename__ = 'role'
    id = db.Column(db.Integer, primary_key=True)
    role = db.Column(db.String(50))
    employees = db.relationship('Employee',backref='post')

    def __init__(self,name) -> None:
        self.name = name


class Employee(db.Model):
    __tablename__ = 'employee'
    id = db.Column(db.Integer, primary_key=True)   
    
    name = db.Column(db.String(80))
    email = db.Column(db.String(80))
    phone = db.Column(db.String(180))
    role_id = db.Column(db.Integer, db.ForeignKey('role.id'))

    def __init__(self,name,email,phone,role_id) -> None:
        self.name = name
        self.email = email
        self.phone = phone
        self.role_id = role_id


with app.app_context():
    db.create_all()   

@app.route('/')
def Index():

    emp_data = db.session.query(Employee.id,Employee.name,Employee.email,Employee.phone, Role.role).join(Role, Role.id == Employee.role_id).all()
    #results = db.session.query(EmployeeRoles).all()

# You can then access the results as a list of objects:
    # for result in results:
    #     print(result.name, result.role)
    return render_template("index.html",employees = emp_data)

@app.route('/insert',methods = ['POST'])
def insert():

    if request.method == 'POST':
        name = request.form['name']
        email = request.form['email']
        phone = request.form['phone']
        role = request.form['role']

        my_data = Employee(name, email,phone,role)
        db.session.add(my_data)
        db.session.commit()

        flash('Employee Inserted successfully')

        return redirect(url_for('Index'))

@app.route('/update',methods=['GET','POST'])
def update():

    if request.method == 'POST':
        my_data = Employee.query.get(request.form.get('id'))

        my_data.name = request.form['name']
        my_data.email = request.form['email']
        my_data.phone = request.form['phone']
        my_data.role_id = request.form['role']

        db.session.commit()
        flash('Employee Updated Successfully')

        return redirect(url_for('Index'))

@app.route('/delete/<id>/',methods = ['GET','POST'])
def delete(id):
      my_data = Employee.query.get(id)
      db.session.delete(my_data)
      db.session.commit()    

      flash("Employee Deleted Successfully")
      return redirect(url_for('Index'))



if __name__ == "__main__":
    app.run(debug=True)
    